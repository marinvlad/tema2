function addTokens(input, tokens) {
    if (typeof input != 'string')
        throw "Invalid input";

    if (input.length < 6)
        throw "Input should have at least 6 characters";

    tokens.forEach(function (item) {
        if (typeof item['tokenName'] != 'string')
            throw "Invalid array format";
    });
    if (!input.includes("...")) {
        return input;
    } else {
        tokens.forEach(function (item) {
            input = input.replace(item.tokenName.charAt(0).toUpperCase()+ item.tokenName.slice(1) + " ...", item.tokenName.charAt(0).toUpperCase()+ item.tokenName.slice(1) + " " + "${"+`${item.tokenName}`+"}");
        });
        return input;
    }

}

const app = {
    addTokens: addTokens
}

module.exports = app;